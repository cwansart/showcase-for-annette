# This script reads every subfolder of the templates folder. Those subfolders
# should contain mustache templates. They get read, converted into mustache
# partials and saved into a generated mustache template for each subfolder.
# Afterwards the generated mustache templates get compiled and saved into a
# dist folder for later usage.

from os import listdir, makedirs
from os.path import isdir, exists, basename, splitext
from glob import glob
from shutil import copytree, copyfile, rmtree
import pystache

# kind of variable folders
templates_path = './templates'
generated_path = './generated'
dist_path = './dist'
assets_path = './assets'
print('templates path is: {}'.format(templates_path))
print('generated path is: {}'.format(generated_path))
print('dist path is: {}'.format(dist_path))
print('assets path is: {}'.format(assets_path))

# remove working dirs and re-create them
rmtree(generated_path, ignore_errors=True)
rmtree(dist_path, ignore_errors=True)
makedirs('{}/pages'.format(generated_path), exist_ok=True)
makedirs(dist_path, exist_ok=True)
makedirs('{}/pages'.format(dist_path), exist_ok=True)

folders = [file for file in listdir(templates_path) if isdir('{}/{}'.format(templates_path, file))]
print('found template folders: {}'.format(folders))

for folder in folders:
  all_templates = glob('{}/{}/*.mustache'.format(templates_path, folder))
  templates_without_heading = [template for template in all_templates if not template.endswith('heading.mustache')]
  print('templates: {}'.format(templates_without_heading))

  import_template = ''

  # add heading.mustache at first if exists
  if exists('{}/{}/heading.mustache'.format(templates_path, folder)):
    import_template += '{{{{> {}/{}/heading.mustache}}}}'.format(templates_path, folder)

  for template in templates_without_heading:
    # this template will be saved in PROJECT_DIR/generated_templates, so we need
    # to get up and into the templates directory
    import_template += '{{{{> {}}}}}'.format(splitext(template)[0])

  with open('{}/pages/{}.mustache'.format(generated_path, folder), 'w', encoding='utf-8') as generated_file:
    generated_file.write(import_template)

# generate navigation links
pages = glob('{}/pages/*.mustache'.format(generated_path))
print('generate navigation links for: {}'.format(pages))

navigation_links = ''
for page in pages:
  page_name = splitext(basename(page))[0]
  navigation_links += '<a href="#page={}">{}</a>'.format(page_name, page_name.capitalize())

with open('{}/navigation_links.mustache'.format(generated_path), 'w', encoding='utf-8') as navigation_file:
    navigation_file.write(navigation_links)

# generate the index.html
with open('{}/index.mustache'.format(templates_path), 'r') as index_template:
  rendered = pystache.render(index_template.read())
  with open('{}/index.html'.format(dist_path), 'w', encoding='utf-8') as index_file:
    index_file.write(rendered)

# generate pages html files
for page in pages:
  with open(page, 'r') as page_template:
    page_name = splitext(basename(page))[0]
    rendered = pystache.render(page_template.read())
    with open('{}/pages/{}.html'.format(dist_path, page_name), 'w', encoding='utf-8') as page_file:
      page_file.write(rendered)

# copy assets into dist folder
copytree(assets_path, '{}/assets'.format(dist_path))
