# Requirements

* python3
* pip3
* pipenv

# Install pipenv

Please check https://docs.pipenv.org/en/latest/install/#installing-pipenv for you specific operating system.

# Install dependend libraries

You need run install the libraries once. On changes of the Pipfile run again:

```
$ pipenv install
```

# Generating dist files

```
$ pipenv run python prepare.py
```

# Run locally

You *need* to run the generation before! Then you can start it locally via

```
$ pipenv run python -m http.server --directory dist
```