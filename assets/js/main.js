$(document).ready(function () {
  $(document).on('pageLoaded', function (_, page) {
    $('<div/>').append(page).find('.anchored').each(function () {
      const id = $(this).attr('id').replace(/\n/, '%20');
      const link = $('<a/>').attr('href', `#${id}`).append(id);
      $('#sub-navigation').empty().append(link);
    });
  });

  $(document).on('pageSelected', function (_, page) {
    console.log('loading page', page);
    // remove slashes to avoid manipulation like adding "../../" and so on.
    const pageWithoutSlashes = page.replace(/\//g, '');
    $('#body-content').load(`./pages/${pageWithoutSlashes}.html`, function (response, status) {
      if (status === 'success') {
        $(document).trigger('pageLoaded', response);
      } else {
        // TODO: add some error handling that is informing the user appropriately
        console.error('Some error occured during loading');
      }
    });
  });

  $(document).on('pageInit', function() {
    // when there is no page given, use 'start'
    console.log('page init', window.location);
    const matches = /#page=(\w+)/.exec(window.location.hash || '');
    console.log('m2', !matches);
    if (!matches || matches.length !== 2) {
      $(document).trigger('pageSelected', 'start');
    } else {
      $(document).trigger('pageSelected', matches[1]);
    }
  });

  // handle initial page loading
  $(document).trigger('pageInit');

  // handle navigation of navigation links
  $('nav > a').click(function () {
    const page = $(this).attr('href')
    const matches = /#page=(\w+)/.exec(page);
    console.log('page, matches', page, matches);

    // to ensure to have the #page=<wanted_page> format we check the size
    if (matches.length === 2) {
      $(document).trigger('pageSelected', matches[1]);
    }
  });
});
